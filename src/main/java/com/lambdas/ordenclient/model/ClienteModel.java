package com.lambdas.ordenclient.model;

import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class ClienteModel {
    
    private String idHash;
    private String apellido;
    private String nombre;
    private String ciRuc;

    public ClienteModel() {
    }

    public ClienteModel(String idHash, String apellido, String nombre) {
        this.idHash = idHash;
        this.apellido = apellido;
        this.nombre = nombre;
    }

    public ClienteModel(String idHash, String apellido, String nombre, String ciRuc) {
        this.idHash = idHash;
        this.apellido = apellido;
        this.nombre = nombre;
        this.ciRuc = ciRuc;
    }

    public String getIdHash() {
        return idHash;
    }

    public void setIdHash(String idHash) {
        this.idHash = idHash;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiRuc() {
        return ciRuc;
    }

    public void setCiRuc(String ciRuc) {
        this.ciRuc = ciRuc;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.idHash);
        hash = 83 * hash + Objects.hashCode(this.apellido);
        hash = 83 * hash + Objects.hashCode(this.nombre);
        hash = 83 * hash + Objects.hashCode(this.ciRuc);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteModel other = (ClienteModel) obj;
        if (!Objects.equals(this.idHash, other.idHash)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return Objects.equals(this.ciRuc, other.ciRuc);
    }

    @Override
    public String toString() {
        return "ClienteModel{" + "idHash=" + idHash + ", apellido=" + apellido + ", nombre=" + nombre + ", ciRuc=" + ciRuc + '}';
    }
    
}
