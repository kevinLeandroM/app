package com.lambdas.ordenclient.model;

import java.util.Objects;
import javax.validation.constraints.Digits;

/**
 *
 * @author kmuniz
 */
public class ArticuloModel {
    
    private String idArticulo;
    private String codigo;
    private String nombre;
    
    @Digits(integer = 8 , fraction = 2)
    private Double precioUnidad;

    public ArticuloModel() {
    }

    public ArticuloModel(String idArticulo, String codigo, String nombre, Double precioUnidad) {
        this.idArticulo = idArticulo;
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioUnidad = precioUnidad;
    }

    public String getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(Double precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.idArticulo);
        hash = 43 * hash + Objects.hashCode(this.codigo);
        hash = 43 * hash + Objects.hashCode(this.nombre);
        hash = 43 * hash + Objects.hashCode(this.precioUnidad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloModel other = (ArticuloModel) obj;
        if (!Objects.equals(this.idArticulo, other.idArticulo)) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return Objects.equals(this.precioUnidad, other.precioUnidad);
    }

    @Override
    public String toString() {
        return "ArticuloModel{" + "idArticulo=" + idArticulo + ", codigo=" + codigo + ", nombre=" + nombre + ", precioUnidad=" + precioUnidad + '}';
    }

}
