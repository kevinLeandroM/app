package com.lambdas.ordenclient.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class OrdenCompraModel {
    
    private String idOrden;
    private LocalDate fechaHora;
    private String idCliente;
    private ClienteModel cliente;
    private Double total;
    private List<DetalleOrdenCompraModel> listDetalle;

    public OrdenCompraModel() {
    }

    public OrdenCompraModel(String idOrden, LocalDate fechaHora, ClienteModel cliente, Double total) {
        this.idOrden = idOrden;
        this.fechaHora = fechaHora;
        this.cliente = cliente;
        this.total = total;
    }

    public OrdenCompraModel(String idOrden, LocalDate fechaHora, ClienteModel cliente, Double total, List<DetalleOrdenCompraModel> listDetalle) {
        this.idOrden = idOrden;
        this.fechaHora = fechaHora;
        this.cliente = cliente;
        this.total = total;
        this.listDetalle = listDetalle;
    }

    public String getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(String idOrden) {
        this.idOrden = idOrden;
    }

    public LocalDate getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(LocalDate fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public ClienteModel getCliente() {
        return cliente;
    }

    public void setCliente(ClienteModel cliente) {
        this.cliente = cliente;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<DetalleOrdenCompraModel> getListDetalle() {
        return listDetalle;
    }

    public void setListDetalle(List<DetalleOrdenCompraModel> listDetalle) {
        this.listDetalle = listDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idOrden);
        hash = 67 * hash + Objects.hashCode(this.fechaHora);
        hash = 67 * hash + Objects.hashCode(this.idCliente);
        hash = 67 * hash + Objects.hashCode(this.cliente);
        hash = 67 * hash + Objects.hashCode(this.total);
        hash = 67 * hash + Objects.hashCode(this.listDetalle);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdenCompraModel other = (OrdenCompraModel) obj;
        if (!Objects.equals(this.idOrden, other.idOrden)) {
            return false;
        }
        if (!Objects.equals(this.idCliente, other.idCliente)) {
            return false;
        }
        if (!Objects.equals(this.fechaHora, other.fechaHora)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.total, other.total)) {
            return false;
        }
        return Objects.equals(this.listDetalle, other.listDetalle);
    }

    @Override
    public String toString() {
        return "OrdenCompraModel{" + "idOrden=" + idOrden + ", fechaHora=" + fechaHora + ", idCliente=" + idCliente + ", cliente=" + cliente + ", total=" + total + ", listDetalle=" + listDetalle + '}';
    }

}
