package com.lambdas.ordenclient.model;

import java.time.LocalTime;
import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class DetalleOrdenCompraModel {
    
    private String id;
    private String idArticulo;
    private ArticuloModel articulo;
    private LocalTime horaRegistro;
    private int cantidad;

    public DetalleOrdenCompraModel() {
    }

    public DetalleOrdenCompraModel(String id, ArticuloModel articulo, LocalTime horaRegistro, int cantidad) {
        this.id = id;
        this.articulo = articulo;
        this.horaRegistro = horaRegistro;
        this.cantidad = cantidad;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public ArticuloModel getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloModel articulo) {
        this.articulo = articulo;
    }

    public LocalTime getHoraRegistro() {
        return horaRegistro;
    }

    public void setHoraRegistro(LocalTime horaRegistro) {
        this.horaRegistro = horaRegistro;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.id);
        hash = 31 * hash + Objects.hashCode(this.idArticulo);
        hash = 31 * hash + Objects.hashCode(this.articulo);
        hash = 31 * hash + Objects.hashCode(this.horaRegistro);
        hash = 31 * hash + this.cantidad;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleOrdenCompraModel other = (DetalleOrdenCompraModel) obj;
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.idArticulo, other.idArticulo)) {
            return false;
        }
        if (!Objects.equals(this.articulo, other.articulo)) {
            return false;
        }
        return Objects.equals(this.horaRegistro, other.horaRegistro);
    }

    @Override
    public String toString() {
        return "DetalleOrdenCompraModel{" + "id=" + id + ", idArticulo=" + idArticulo + ", articulo=" + articulo + ", horaRegistro=" + horaRegistro + ", cantidad=" + cantidad + '}';
    }
    
}
