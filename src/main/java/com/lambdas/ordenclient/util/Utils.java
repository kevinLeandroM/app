package com.lambdas.ordenclient.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author kmuniz
 */
public class Utils {

    private static final String C_dES = "DESede!";
    
    private  static final String secretKey = "nkjabshk9u1145sa";
    
    public static String encode(String cadena) {
        String generated = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] llavePasword = md.digest(secretKey.getBytes());
            byte[] byteKey = Arrays.copyOf(llavePasword, 24);
            SecretKey key = new SecretKeySpec(byteKey, C_dES);
            Cipher cifrado = Cipher.getInstance(C_dES);
            cifrado.init(Cipher.ENCRYPT_MODE, key);
            byte[] plaintText = cadena.getBytes("utf-8");
            byte[] buf = cifrado.doFinal(plaintText);
            byte[] base64Byte = Base64.encodeBase64(buf);
            generated = new String(base64Byte);
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            logCreate("ERROR", "No Such algorithm: " + e.getMessage());
        }
        return generated;
    }
    
    public static String decode(String cadena){
        String generated = "";
        try {
            byte[] message = Base64.decodeBase64(cadena.getBytes());
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md5.digest(secretKey.getBytes());
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes,C_dES);
            Cipher decipher = Cipher.getInstance(C_dES);
            decipher.init(Cipher.ENCRYPT_MODE,key);
            byte[] plaintText = decipher.doFinal(message);
            generated = new String(plaintText);
        }catch (InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            logCreate("ERROR", "No Such algorithm: " + e.getMessage());
        }
        return generated;
    }
    
    public static void logCreate(String type, Object error){
        System.out.println(type.toUpperCase().concat(": ").concat(error.toString()));
    }
    
    public static String toMaxLongitud(String data, int longitud){
        return (data.length() > longitud)?data.substring(0,longitud-1):data;
    }
    
    public static boolean doubleInRange(Double in){
        String[] descPrice = in.toString().replace(".", Constant.ERR).split(Constant.ERR,3);
        return (descPrice[0].length() > Constant.ENTERO_MAX || descPrice[1].length() > Constant.DECIMAL_MAX);
    }
    
}
