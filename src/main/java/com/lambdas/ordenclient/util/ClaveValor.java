package com.lambdas.ordenclient.util;

import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class ClaveValor {
    
    private Object clave;
    private Object valor;

    public ClaveValor() {
    }

    public ClaveValor(Object clave) {
        this.clave = clave;
    }

    public ClaveValor(Object clave, Object valor) {
        this.clave = clave;
        this.valor = valor;
    }

    public Object getClave() {
        return clave;
    }

    public void setClave(Object clave) {
        this.clave = clave;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.clave);
        hash = 71 * hash + Objects.hashCode(this.valor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClaveValor other = (ClaveValor) obj;
        if (!Objects.equals(this.clave, other.clave)) {
            return false;
        }
        return Objects.equals(this.valor, other.valor);
    }

    @Override
    public String toString() {
        return "ClaveValor{" + "clave=" + clave + ", valor=" + valor + '}';
    }
    
}
