package com.lambdas.ordenclient.util;

/**
 *
 * @author kmuniz
 */
public class Constant {
    
    public static final String SIN_REGISTROS = "No se encontraron registros.";
    public static final String GET_EXITO = "Se realizo la consulta exitosamente.";
    public static final String ERROR_INTERNO = "Algo salio mal, por favor comunicate con el Administrador del sistema";
    public static final String ERROR_API_EXTERNO = "No se logro la cominicación con la API: ";
    public static final String ERROR_CONERVSION_ENTITY = "No se logro convertir a la entidad. ";
    public static final String CREATE_ENTITY = "Se registro exitodamente la nueva entidad. ";
    public static final String UPDATE_ENTITY = "Se actualizacion exitodamente la entidad. ";
    public static final String DELETE_ENTITY = "Se elimino exitodamente la entidad. ";
    public static final String REPEAT_ENTITY = "El registro ya existe en la entidad. ";
    public static final String CAMPO_VACIO = "El campo no puede estar vacio:";
    public static final String CAMPOS_VACIOS = "Los campos no pueden estar vacio:";
    public static final String ENTIDAD_UNKLOAD = "No se encuentra la entidad:";
    public static final String PRECIO_MENOR = "El precio no puede ser menor a 0";
    public static final String LONGITUD_EXCEDIDA = "No puede exceder la lingutd de ";
    
    public static final String ERR = "ERROR";
    
    public static final int NAME_FL_LONG = 80;
    public static final int CODIG_ARTIC_LONG = 20;
    
    public static final int ENTERO_MAX = 8;
    public static final int DECIMAL_MAX = 2;
    
}
