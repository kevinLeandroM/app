package com.lambdas.ordenclient.util;

import java.util.Optional;
import javax.ws.rs.core.Response;

/**
 *
 * @author kmuniz
 */
public class ResponseBuilder {
    
    public static Response responseSingle(Response.Status status, String message){
        return Response.status(status)
                .entity(new ServerResponse(status.getStatusCode(), status.name(), message)).build();
    }
    
    public static Response responseObject(Response.Status status, String message, Object entity){
        return Response.status(status)
                .entity(new ServerResponse(status.getStatusCode(), status.name(), message, entity)).build();
    }
    
    public static Response responseNullSingle(Object entity){
        return Optional.ofNullable(entity)
                .map(result -> responseSingle(Response.Status.OK, MessageReponse.getExitoso(entity.getClass().getName())))
                .orElse(responseSingle(Response.Status.BAD_REQUEST, MessageReponse.sinResgistro(entity.getClass().getName())));
    }
    
    public static Response responseNullObject(Object entity){
        return Optional.ofNullable(entity)
                .map(result -> responseObject(Response.Status.OK, MessageReponse.getExitoso(entity.getClass().getName()),entity))
                .orElse(responseObject(Response.Status.BAD_REQUEST, MessageReponse.sinResgistro(entity.getClass().getName()),entity));
    }
    
}
