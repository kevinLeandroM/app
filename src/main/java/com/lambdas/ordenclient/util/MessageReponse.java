package com.lambdas.ordenclient.util;

import java.util.List;

/**
 *
 * @author kmuniz
 */
public class MessageReponse {    
    
    public static String sinResgistro(String entidad){
        return Constant.SIN_REGISTROS.concat(" En la entidad ").concat(entidad.toUpperCase());
    }
    
    public static String getExitoso(String entidad){
        return Constant.GET_EXITO.concat(" En la entidad ").concat(entidad.toUpperCase());
    }
    
    public static String errorInterno(){
        return Constant.ERROR_INTERNO;
    }
    
    public static String errorApiExterno(String entidad,String messageError){
        return Constant.ERROR_API_EXTERNO.concat(entidad.toUpperCase()).concat(". Causa").concat(messageError);
    }
    
    public static String errorConversionEntity(String messageError){
        return Constant.ERROR_CONERVSION_ENTITY.concat(". Causa").concat(messageError);
    }
    
    public static String createEntity(String entidad){
        return Constant.CREATE_ENTITY.concat(" ").concat(entidad.toUpperCase());
    }
    
    public static String updateEntity(String entidad){
        return Constant.UPDATE_ENTITY.concat(" ").concat(entidad.toUpperCase());
    }
    
    public static String deleteEntity(String entidad){
        return Constant.DELETE_ENTITY.concat(" ").concat(entidad.toUpperCase());
    }
    
    public static String repeatEntity(String entidad){
        return Constant.REPEAT_ENTITY.concat(" ").concat(entidad.toUpperCase());
    }
    
    public static String campoVacio(String nameTag){
        return Constant.CAMPO_VACIO.concat(" ").concat(nameTag.toUpperCase());
    }
    
    public static String camposVacios(List<String> listNameTag){
        String out = Constant.CAMPOS_VACIOS.concat(" ");
        int contandor = 0;
        for(String item : listNameTag){
            out += (contandor==0?item:" ó ".concat(item));
            contandor++;
        }
        return out;
    }
    
    public static String entidadUnkload(String entidad){
        return Constant.ENTIDAD_UNKLOAD.concat(" ").concat(entidad.toUpperCase());
    }
    
    public static String longitudExcedida(String tag, int longitud){
        return Constant.LONGITUD_EXCEDIDA.concat(longitud+"").concat(" en el campo").concat(tag.toUpperCase());
    }
    
    public static String longitudExcedidaDouble(String tag){
        return Constant.LONGITUD_EXCEDIDA.concat(" ENTERO: "+Constant.ENTERO_MAX+" DECIMAL: "+Constant.DECIMAL_MAX).concat(" en el campo ").concat(tag.toUpperCase());
    }
    
}
