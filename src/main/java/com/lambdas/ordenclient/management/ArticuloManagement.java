package com.lambdas.ordenclient.management;

import com.lambdas.ordenclient.domain.Articulo;
import com.lambdas.ordenclient.model.ArticuloModel;
import com.lambdas.ordenclient.service.ArticuloService;
import com.lambdas.ordenclient.util.Constant;
import com.lambdas.ordenclient.util.MessageReponse;
import com.lambdas.ordenclient.util.ResponseBuilder;
import com.lambdas.ordenclient.util.Utils;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

/**
 *
 * @author kmuniz
 */
public class ArticuloManagement {
    
    @Inject
    private ArticuloService articuloService;
    
    private final static String ENTITY = "Articulo";
    
    public Response findAll() {
        try {
            List<Articulo> listArticulo = articuloService.getAll();
            if(listArticulo.isEmpty() || listArticulo == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.sinResgistro(ENTITY));
            }
            
            List<ArticuloModel> listModel = new ArrayList<>();
            listArticulo.forEach(item -> {
                listModel.add(toModelFront(item));
            });
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.getExitoso(ENTITY), listModel);
        } catch (Exception e ){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response findID(String identifer) {
        try {
            if(identifer == null || "".equals(identifer)){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("IDENTIFIER"));
            }
            ArticuloModel out = toModelFront(articuloService.findId(Long.parseLong(identifer)));
            if(out.getIdArticulo() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.sinResgistro(ENTITY));
            }
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.getExitoso(ENTITY), out);
        } catch (NumberFormatException e ){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response create(ArticuloModel model) {
        try {
            //<editor-fold defaultstate="collapsed" desc="Validacion del Modelo">
            if(model.getCodigo()== null || "".equals(model.getCodigo())){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("NOMBRE"));
            }  else if(model.getCodigo().length() > Constant.CODIG_ARTIC_LONG){
                return ResponseBuilder.responseSingle(Response.Status.CONFLICT, 
                        MessageReponse.longitudExcedida("Codigo",Constant.CODIG_ARTIC_LONG));
            } else if (model.getNombre()== null || "".equals(model.getNombre())){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("APELLIDO"));
            } else if(model.getNombre().length() > Constant.NAME_FL_LONG){
                return ResponseBuilder.responseSingle(Response.Status.CONFLICT, 
                        MessageReponse.longitudExcedida("APELLIDO",Constant.NAME_FL_LONG));
            } else if (model.getPrecioUnidad() == null || model.getPrecioUnidad() < 0){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        Constant.PRECIO_MENOR);
            } else if(model.getPrecioUnidad().isNaN()){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("PRECIO"));
            } else if(Utils.doubleInRange(model.getPrecioUnidad())){
                    return ResponseBuilder.responseSingle(Response.Status.CONFLICT, 
                        MessageReponse.longitudExcedidaDouble("PRECIO"));
            }else if(!articuloService.findArticuloByCode(model.getCodigo()).isEmpty()){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.repeatEntity(ENTITY));
            }
            //</editor-fold>
            articuloService.create(toEntity(model));
            return ResponseBuilder.responseObject(Response.Status.CREATED, 
                    MessageReponse.createEntity(ENTITY), toModelFront(articuloService.findLastArticulo().get()));
        } catch (Exception e ){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public ArticuloModel toModelFront(Articulo in){
        return new ArticuloModel(in.getIdArticulo().toString(), in.getCodigoArticulo()
                ,in.getNombre(),in.getPrecioUnitario());
    }
    
    public Articulo toEntity(ArticuloModel in){
        return new Articulo(in.getCodigo(), in.getNombre(), in.getPrecioUnidad());
    }
    
    public Articulo toEntityFull(ArticuloModel in){
        return new Articulo(Long.parseLong(in.getIdArticulo()),in.getCodigo(), in.getNombre(), in.getPrecioUnidad());
    }
    
}
