package com.lambdas.ordenclient.management;

import com.lambdas.ordenclient.domain.Cliente;
import com.lambdas.ordenclient.model.ClienteModel;
import com.lambdas.ordenclient.service.ClienteService;
import com.lambdas.ordenclient.util.Constant;
import com.lambdas.ordenclient.util.MessageReponse;
import com.lambdas.ordenclient.util.ResponseBuilder;
import com.lambdas.ordenclient.util.Utils;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

/**
 *
 * @author kmuniz
 */
public class ClienteManagement {
    
    @Inject
    private ClienteService clienteService;
    
    private final static String ENTITY = "Cliente";
    
    public Response getListCliente(){
        try {
            List<Cliente> listClient = clienteService.getListCliente();
            if(listClient.isEmpty() || listClient == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.sinResgistro(ENTITY));
            }
            List<ClienteModel> listModel = new ArrayList<>();
            listClient.forEach(item -> {
                listModel.add(toModelFront(item));
            });
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.getExitoso(ENTITY), listModel);
        } catch (Exception e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response createCliente(ClienteModel model) {
        try {
            //<editor-fold defaultstate="collapsed" desc="Validacion del Modelo">
            if(model.getNombre()== null || "".equals(model.getNombre())){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("NOMBRE"));
            } else if (model.getApellido()== null || "".equals(model.getApellido())){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("APELLIDO"));
            }
            model.setNombre(Utils.toMaxLongitud(model.getNombre(), Constant.NAME_FL_LONG));
            model.setApellido(Utils.toMaxLongitud(model.getApellido(), Constant.NAME_FL_LONG));
            //</editor-fold>
            clienteService.createCliente(toEntity(model));
            return ResponseBuilder.responseObject(Response.Status.CREATED, 
                    MessageReponse.createEntity(ENTITY), toModelFront(clienteService.findLastIngredient().get()));
        } catch (Exception e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public ClienteModel toModelFront(Cliente in){
        return new ClienteModel(in.getIdCliente().toString(), 
                in.getApellido(), in.getNombre(), in.getCiRuc()!=null?in.getCiRuc():"");
    }
    
    public Cliente toEntity(ClienteModel in){
        return new Cliente(in.getNombre(), in.getApellido(),
                ( in.getCiRuc()!=null || !"".equals(in.getCiRuc()) )?in.getCiRuc():null);
    }
    
    public Cliente toEntityFull(ClienteModel in){
        return new Cliente(Long.parseLong(in.getIdHash()),in.getNombre(), in.getApellido(),
                ( in.getCiRuc()!=null || !"".equals(in.getCiRuc()) )?in.getCiRuc():null);
    }
    
}
