package com.lambdas.ordenclient.management;

import com.lambdas.ordenclient.domain.Articulo;
import com.lambdas.ordenclient.domain.DetalleOrdenCompra;
import com.lambdas.ordenclient.domain.OrdenCompra;
import com.lambdas.ordenclient.model.DetalleOrdenCompraModel;
import com.lambdas.ordenclient.model.OrdenCompraModel;
import com.lambdas.ordenclient.service.ArticuloService;
import com.lambdas.ordenclient.service.ClienteService;
import com.lambdas.ordenclient.service.OrdenCompraService;
import com.lambdas.ordenclient.util.Constant;
import com.lambdas.ordenclient.util.MessageReponse;
import com.lambdas.ordenclient.util.ResponseBuilder;
import com.lambdas.ordenclient.util.Utils;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author kmuniz
 */
public class OrdenCompraManagement {
    
    @Inject
    private OrdenCompraService ordenCompraService;
    
    @Inject
    private ArticuloService articuloService;
    
    @Inject
    private ClienteService clienteService;
    
    @Inject
    private ClienteManagement clienteManagement;
    
    @Inject
    private ArticuloManagement articuloManagement;
    
    private final static String ENTITY = "Orden de Compra";
    
    public Response findAll() {
        try {
            List<OrdenCompra> listOrdenCompra = ordenCompraService.getAll();
            if(listOrdenCompra.isEmpty() || listOrdenCompra == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.sinResgistro(ENTITY));
            }
            List<OrdenCompraModel> listModel = new ArrayList<>();
            listOrdenCompra.forEach(item -> {
                listModel.add(toModelFront(item));
            });
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.getExitoso(ENTITY), listModel);
        } catch (Exception e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response findID(String identifier) {
        try{
            if(identifier == null || "".equals(identifier)) {
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("ID"));
            }
            OrdenCompra orden = ordenCompraService.finCabeceraID(Long.parseLong(identifier));
            if(orden.getIdOrdenCompra() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("ORDEN DE COMPRA"));
            }
            OrdenCompraModel model = toModelFront(orden);
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.getExitoso(ENTITY), model);
        } catch (NumberFormatException e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response create(OrdenCompraModel model) {
        try {
            //<editor-fold defaultstate="collapsed" desc="Validacion del Modelo">
            if (model.getIdCliente() == null || "".equals(model.getIdCliente())){
                 return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("CLIENTE"));
            } else if(clienteService.findId(Long.parseLong(model.getIdCliente())).getIdCliente() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("CLIENTE"));
            }else if(model.getListDetalle() == null || model.getListDetalle().isEmpty()){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("LISTA DE ARTICULO"));
            }            
            Double sumTotal = 0.0;
            for(DetalleOrdenCompraModel item : model.getListDetalle()){
                if (item.getIdArticulo() == null || "".equals(item.getIdArticulo())){
                    return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("ARTICULO"));
                } else if(articuloService.findId(Long.parseLong(item.getIdArticulo())).getIdArticulo() == null){
                    return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("ARTICULO"));
                } else if(item.getCantidad() <= 0){
                    return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                            MessageReponse.campoVacio("CANTIDAD"));
                } 
                item.setArticulo(articuloManagement.toModelFront(articuloService.findId(Long.parseLong(item.getIdArticulo()))));
                sumTotal = sumTotal + (item.getArticulo().getPrecioUnidad() * item.getCantidad());
            }
            model.setTotal(sumTotal);    
            model.setCliente(clienteManagement.toModelFront(clienteService.findId(Long.parseLong(model.getIdCliente()))));
            //</editor-fold>
            ordenCompraService.createOrderAndDetail(model);
            return ResponseBuilder.responseObject(Response.Status.CREATED, 
                    MessageReponse.createEntity(ENTITY), toModelFront(ordenCompraService.findLastOrdenCompra().get()));
        } catch (NumberFormatException e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response ordenDetail(String id) {
        try {
            if(id == null || "".equals(id)) {
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("ID"));
            }
            OrdenCompra order = ordenCompraService.finCabeceraID(Long.parseLong(id));
            if(order.getIdOrdenCompra() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("ORDEN COMPRA"));
            }
            List<DetalleOrdenCompra> listDetail = ordenCompraService.findDetailOrderCompraByIDOrder(order.getIdOrdenCompra());
            listDetail.forEach(item->{
                ordenCompraService.deleteDetailOrder(ordenCompraService.finDetailID(item.getIdDetalleOrdenCompra()));
            });
            ordenCompraService.deleteOrderHead(order);
            return ResponseBuilder.responseSingle(Response.Status.OK, 
                    MessageReponse.updateEntity(ENTITY));
        } catch (NumberFormatException e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response deleteDetail(String id) {
        try {
            //<editor-fold defaultstate="collapsed" desc="Validacion del Modelo">
            if(id == null || "".equals(id)) {
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("ID"));
            } 
            DetalleOrdenCompra detailDEL = ordenCompraService.finDetailID(Long.parseLong(id));
            if (detailDEL.getIdDetalleOrdenCompra()== null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("DETALLE ORDEN COMPRA"));
            } 
            //</editor-fold>
            OrdenCompra inBD = ordenCompraService.finCabeceraID(detailDEL.getCabeceraOrdenCompra().getIdOrdenCompra());
            ordenCompraService.deleteDetailOrder(detailDEL);
            List<DetalleOrdenCompra> listDetail = ordenCompraService.findDetailOrderCompraByIDOrder(inBD.getIdOrdenCompra());
            if(listDetail.isEmpty()){
                ordenCompraService.deleteOrderHead(inBD);
                return ResponseBuilder.responseSingle(Response.Status.OK, 
                    MessageReponse.deleteEntity(ENTITY));
            }else{
                Double sumTotal = 0.0;
                for(DetalleOrdenCompra item : listDetail){
                    sumTotal = sumTotal + (item.getArticulo().getPrecioUnitario() * item.getCantidad());
                }
                inBD.setTotal(sumTotal);
                ordenCompraService.updateOrderHead(inBD);
                return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.updateEntity(ENTITY), toModelFront(ordenCompraService.finCabeceraID(inBD.getIdOrdenCompra())));
            }
        } catch (NumberFormatException e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public Response updateDetail(DetalleOrdenCompraModel entity) {
        try {
            if (entity.getIdArticulo() == null || "".equals(entity.getIdArticulo())){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                    MessageReponse.campoVacio("ARTICULO"));
            } else if(articuloService.findId(Long.parseLong(entity.getIdArticulo())).getIdArticulo() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                    MessageReponse.entidadUnkload("ARTICULO"));
            } else if(entity.getCantidad() <= 0){
                return ResponseBuilder.responseSingle(Response.Status.NO_CONTENT, 
                        MessageReponse.campoVacio("CANTIDAD"));
            }
            DetalleOrdenCompra detail = ordenCompraService.finDetailID(Long.parseLong(entity.getId()));
            if (detail.getIdDetalleOrdenCompra() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("DETALLE ORDEN COMPRA"));
            }
            Articulo articuloNew = articuloService.findId(Long.parseLong(entity.getIdArticulo()));
            if(articuloNew.getIdArticulo() == null){
                return ResponseBuilder.responseSingle(Response.Status.BAD_REQUEST, 
                        MessageReponse.entidadUnkload("ARTICULO"));
            }
            detail.setArticulo(articuloNew);
            detail.setCantidad(entity.getCantidad());
            ordenCompraService.updateDetalOrder(detail);
            OrdenCompra ordenCompra = ordenCompraService.finCabeceraID(detail.getCabeceraOrdenCompra().getIdOrdenCompra());
            List<DetalleOrdenCompra> listDetail = ordenCompraService.findDetailOrderCompraByIDOrder(ordenCompra.getIdOrdenCompra());
            Double sumTotal = 0.0;
            for(DetalleOrdenCompra item : listDetail){
                sumTotal = sumTotal + (item.getArticulo().getPrecioUnitario() * item.getCantidad());
            }
            ordenCompra.setTotal(sumTotal);
            ordenCompraService.updateOrderHead(ordenCompra);
            return ResponseBuilder.responseObject(Response.Status.OK, 
                    MessageReponse.updateEntity(ENTITY), toModelFront(ordenCompraService.finCabeceraID(detail.getCabeceraOrdenCompra().getIdOrdenCompra())));
        } catch (NumberFormatException e){
            Utils.logCreate(Constant.ERR, e.getMessage()!=null?e.getMessage():e);
            return ResponseBuilder.responseObject(Response.Status.INTERNAL_SERVER_ERROR, 
                    MessageReponse.errorInterno(), e);
        }
    }
    
    public OrdenCompraModel toModelFront(OrdenCompra in){
        OrdenCompraModel out = new OrdenCompraModel(in.getIdOrdenCompra().toString(), 
                in.getFechaHora(), clienteManagement.toModelFront(in.getCliente()), in.getTotal());
        out.setListDetalle(listDetailToModel(ordenCompraService.findDetailOrderCompraByIDOrder(in.getIdOrdenCompra())));
        return out;
    }
    
    public List<DetalleOrdenCompraModel> listDetailToModel( List<DetalleOrdenCompra> in){
        List<DetalleOrdenCompraModel> out = new ArrayList<>();
        if(in == null){
            return out;
        }
        in.forEach((DetalleOrdenCompra item) -> {
            out.add(detailToModel(item));
        });
        return out;
    }
    
    public DetalleOrdenCompraModel detailToModel(DetalleOrdenCompra in){
        return new DetalleOrdenCompraModel(in.getIdDetalleOrdenCompra().toString(),
                articuloManagement.toModelFront(in.getArticulo()), in.getHoraRegis(), in.getCantidad());
    }
    
}
