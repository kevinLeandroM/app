package com.lambdas.ordenclient.controller;

import com.lambdas.ordenclient.management.OrdenCompraManagement;
import com.lambdas.ordenclient.model.DetalleOrdenCompraModel;
import com.lambdas.ordenclient.model.OrdenCompraModel;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

/**
 *
 * @author kmuniz
 */
@Stateless
@Path("/purchase-order")
public class OrdenCompraController {
    
    @Inject
    private OrdenCompraManagement ordenCompraManagement;
    
    @Timed
    @Operation(summary = "Obtener el listado de Ordenes de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return ordenCompraManagement.findAll();
    }
    
    @Timed
    @Operation(summary = "Obtener el listado de Ordenes de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @GET
    @Path("/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findID(@PathParam("identifier") String identifier) {
        return ordenCompraManagement.findID(identifier);
    }
    
    @Timed
    @Operation(summary = "Crear una nueva Ordenes de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(OrdenCompraModel model) {
        return ordenCompraManagement.create(model);
    }

    @Timed
    @Operation(summary = "Eliminar un detalle de la Orden de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @DELETE
    @Path("/{idEntity}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ordenDetail(@PathParam("idEntity") String id) {
        return ordenCompraManagement.ordenDetail(id);
    }    
    
    @Timed
    @Operation(summary = "Eliminar un detalle de la Orden de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @DELETE
    @Path("/detail/{idEntity}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDetail(@PathParam("idEntity") String id) {
        return ordenCompraManagement.deleteDetail(id);
    }
    
    @Timed
    @Operation(summary = "Eliminar un detalle de la Orden de compra.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @PUT
    @Path("/detail")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDetail(DetalleOrdenCompraModel entity) {
        return ordenCompraManagement.updateDetail(entity);
    }
    
}
