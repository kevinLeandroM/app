package com.lambdas.ordenclient.controller;

import com.lambdas.ordenclient.management.ArticuloManagement;
import com.lambdas.ordenclient.model.ArticuloModel;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

/**
 *
 * @author kmuniz
 */
@Stateless
@Path("/article")
public class ArticuloController {
    
    @Inject
    private ArticuloManagement articuloManagement;
    
    @Timed
    @Operation(summary = "Obtener el listado de Clientes.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return articuloManagement.findAll();
    }
    
    @Timed
    @Operation(summary = "Obtener el listado de Clientes.")
    @APIResponse(responseCode = "200", description = "OK")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @GET
    @Path("/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findID(@PathParam("identifier") String identifer) {
        return articuloManagement.findID(identifer);
    }
    
    @Timed
    @Operation(summary = "Crear un nuevo Cliente.")
    @APIResponse(responseCode = "200", description = "Created")
    @APIResponse(responseCode = "400", description = "Bad Request")
    @APIResponse(responseCode = "500", description = "Internal Server Error")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(ArticuloModel entity) {
        return articuloManagement.create(entity);
    }
    
}
