package com.lambdas.ordenclient.service;

import com.lambdas.ordenclient.domain.Cliente;
import com.lambdas.ordenclient.repository.ClienteReporitory;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author kmuniz
 */
public class ClienteService {
    
    @Inject
    private ClienteReporitory clienteReporitory;
    
    public Cliente findId(Long idCli){
        return clienteReporitory.find(idCli);
    }
    
    public List<Cliente> getListCliente(){
        return clienteReporitory.findAll();
    }
    
    public void createCliente(Cliente in){
        clienteReporitory.create(in);
    }
    
    public Optional<Cliente> findLastIngredient(){
        return clienteReporitory.findNumberByNamedQuery("findLastClient",1);
    }
}
