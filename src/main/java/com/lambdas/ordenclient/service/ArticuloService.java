package com.lambdas.ordenclient.service;

import com.lambdas.ordenclient.domain.Articulo;
import com.lambdas.ordenclient.repository.ArticuloRepository;
import static java.util.Collections.singletonMap;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author kmuniz
 */
public class ArticuloService {
    
    @Inject
    private ArticuloRepository articuloRepository;
    
    public Articulo findId(Long idArt){
        return articuloRepository.find(idArt);
    }
    
    public List<Articulo> getAll(){
        return articuloRepository.findAll();
    }
    
    public void create(Articulo entity){
        articuloRepository.create(entity);
    }
    
    public Optional<Articulo> findLastArticulo(){
        return articuloRepository.findNumberByNamedQuery("findArticuloDes",1);
    }
    
    public  List<Articulo> findArticuloByCode(String codigo){
        return articuloRepository.findByNamedQuery("findArticuloByCode", singletonMap("codeIn", codigo));
    }
    
}
