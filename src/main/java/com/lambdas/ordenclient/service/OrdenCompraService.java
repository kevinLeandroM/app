package com.lambdas.ordenclient.service;

import com.lambdas.ordenclient.domain.DetalleOrdenCompra;
import com.lambdas.ordenclient.domain.OrdenCompra;
import com.lambdas.ordenclient.management.ArticuloManagement;
import com.lambdas.ordenclient.management.ClienteManagement;
import com.lambdas.ordenclient.model.OrdenCompraModel;
import com.lambdas.ordenclient.repository.OrdenCompraCabRepository;
import com.lambdas.ordenclient.repository.OrdenCompraDetRepository;
import java.time.LocalDate;
import java.time.LocalTime;
import static java.util.Collections.singletonMap;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

/**
 *
 * @author kmuniz
 */
public class OrdenCompraService {
    
    @Inject
    private OrdenCompraCabRepository cabeceraRepository;
    
    @Inject
    private OrdenCompraDetRepository detalleRepository;
    
    @Inject
    private ClienteManagement clienteManagement;
    
    @Inject
    private ArticuloManagement articuloManagement;
    
    public List<OrdenCompra> getAll(){
        return cabeceraRepository.findAll();
    }
    
    public OrdenCompra finCabeceraID(Long identifier){
        return cabeceraRepository.find(identifier);
    }
    
    public DetalleOrdenCompra finDetailID(Long identifier){
        return detalleRepository.find(identifier);
    }
    
    public Optional<OrdenCompra> findLastOrdenCompra(){
        return cabeceraRepository.findNumberByNamedQuery("findOrderCompraDescById",1);
    }
    
    public  List<DetalleOrdenCompra> findDetailOrderCompraByIDOrder(Long identifier){
        return detalleRepository.findByNamedQuery("findDetailOrderCompraByIDOrder", singletonMap("cabecera", identifier));
    }
    
    public void createOrderAndDetail(OrdenCompraModel in){
        in.setFechaHora(LocalDate.now());
        createOrderHead(new OrdenCompra(in.getFechaHora(), clienteManagement.toEntityFull(in.getCliente()), in.getTotal()));
        OrdenCompra inBD = findLastOrdenCompra().get();
        in.getListDetalle().forEach(item -> {
            createOrderVoid(new DetalleOrdenCompra(inBD,articuloManagement.toEntityFull(item.getArticulo()),LocalTime.now(),item.getCantidad()));
        });
    }
    
    public void createOrderHead(OrdenCompra in){
        cabeceraRepository.create(in);
    }
    
    public void updateOrderHead(OrdenCompra entity){
        cabeceraRepository.edit(entity);
    }
    
    public void deleteOrderHead(OrdenCompra entity){
        cabeceraRepository.remove(entity);
    }
    
    public void createOrderVoid(DetalleOrdenCompra in){
        detalleRepository.create(in);
    }
    
    public void deleteDetailOrder(DetalleOrdenCompra entity){
        detalleRepository.remove(entity);
    }
    
    public void updateDetalOrder(DetalleOrdenCompra entity){
        detalleRepository.edit(entity);
    }
    
}
