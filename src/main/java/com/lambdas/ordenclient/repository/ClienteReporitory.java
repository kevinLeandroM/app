package com.lambdas.ordenclient.repository;

import com.lambdas.ordenclient.domain.Cliente;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kmuniz
 */
public class ClienteReporitory extends AbstractFacade<Cliente, Long> {

    @PersistenceContext(unitName = "DEFAULT.0PU")
    private EntityManager em;

    public ClienteReporitory() {
        super(Cliente.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
