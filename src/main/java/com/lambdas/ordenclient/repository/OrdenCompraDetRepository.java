package com.lambdas.ordenclient.repository;

import com.lambdas.ordenclient.domain.DetalleOrdenCompra;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kmuniz
 */
public class OrdenCompraDetRepository extends AbstractFacade<DetalleOrdenCompra, Long> {

    @PersistenceContext(unitName = "DEFAULT.0PU")
    private EntityManager em;

    public OrdenCompraDetRepository() {
        super(DetalleOrdenCompra.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
