package com.lambdas.ordenclient.repository;

import com.lambdas.ordenclient.domain.Articulo;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kmuniz
 */
public class ArticuloRepository extends AbstractFacade<Articulo, Long> {

    @PersistenceContext(unitName = "DEFAULT.0PU")
    private EntityManager em;

    public ArticuloRepository() {
        super(Articulo.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
