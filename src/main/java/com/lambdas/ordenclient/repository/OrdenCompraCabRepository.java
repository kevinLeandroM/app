package com.lambdas.ordenclient.repository;

import com.lambdas.ordenclient.domain.OrdenCompra;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kmuniz
 */
public class OrdenCompraCabRepository extends AbstractFacade<OrdenCompra, Long> {

    @PersistenceContext(unitName = "DEFAULT.0PU")
    private EntityManager em;

    public OrdenCompraCabRepository() {
        super(OrdenCompra.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
