package com.lambdas.ordenclient.domain;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author kmuniz
 */

@Entity
@Table(name = "ORDER_COMPR_DET_PRODUCT", schema = "producer")
@NamedQuery(name = "findDetailOrderCompraByIDOrder", query = "SELECT doc FROM DetalleOrdenCompra doc WHERE doc.cabeceraOrdenCompra.idOrdenCompra = :cabecera ORDER BY doc.horaRegis")
public class DetalleOrdenCompra implements Serializable{
    
    @Id
    @GeneratedValue(generator = "seq_id_orderCompraDet_product", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "seq_id_orderCompraDet_product", sequenceName = "seq_id_orderCompraDet_product", allocationSize = 19, schema = "producer")
    @Column(name = "ID_ORDER_DET_COMPRA")
    private Long idDetalleOrdenCompra;
    
    @ManyToOne
    @JoinColumn(name = "ID_CAB_ORDER_COMPRA", referencedColumnName = "ID_ORDER_COMPRA")
    private OrdenCompra cabeceraOrdenCompra;
    
    @ManyToOne
    @JoinColumn(name = "ID_ARTICULO_ORDER_COMPRA", referencedColumnName = "ID_ARTICU")
    private Articulo articulo;
    
    @Basic
    @Column(name = "HORA_REGIS")
    private LocalTime horaRegis;
    
    @Basic
    @Column(name = "CANT_ARTIC")
    private int cantidad;

    public DetalleOrdenCompra() {
    }

    public DetalleOrdenCompra(OrdenCompra cabeceraOrdenCompra, Articulo articulo, LocalTime horaRegis, int cantidad) {
        this.cabeceraOrdenCompra = cabeceraOrdenCompra;
        this.articulo = articulo;
        this.horaRegis = horaRegis;
        this.cantidad = cantidad;
    }
    
    public Long getIdDetalleOrdenCompra() {
        return idDetalleOrdenCompra;
    }

    public void setIdDetalleOrdenCompra(Long idDetalleOrdenCompra) {
        this.idDetalleOrdenCompra = idDetalleOrdenCompra;
    }

    public OrdenCompra getCabeceraOrdenCompra() {
        return cabeceraOrdenCompra;
    }

    public void setCabeceraOrdenCompra(OrdenCompra cabeceraOrdenCompra) {
        this.cabeceraOrdenCompra = cabeceraOrdenCompra;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public LocalTime getHoraRegis() {
        return horaRegis;
    }

    public void setHoraRegis(LocalTime horaRegis) {
        this.horaRegis = horaRegis;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.idDetalleOrdenCompra);
        hash = 41 * hash + Objects.hashCode(this.cabeceraOrdenCompra);
        hash = 41 * hash + Objects.hashCode(this.articulo);
        hash = 41 * hash + Objects.hashCode(this.horaRegis);
        hash = 41 * hash + this.cantidad;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleOrdenCompra other = (DetalleOrdenCompra) obj;
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (!Objects.equals(this.idDetalleOrdenCompra, other.idDetalleOrdenCompra)) {
            return false;
        }
        if (!Objects.equals(this.cabeceraOrdenCompra, other.cabeceraOrdenCompra)) {
            return false;
        }
        if (!Objects.equals(this.articulo, other.articulo)) {
            return false;
        }
        return Objects.equals(this.horaRegis, other.horaRegis);
    }

    @Override
    public String toString() {
        return "DetalleOrdenCompra{" + "idDetalleOrdenCompra=" + idDetalleOrdenCompra + ", cabeceraOrdenCompra=" + cabeceraOrdenCompra + ", articulo=" + articulo + ", horaRegis=" + horaRegis + ", cantidad=" + cantidad + '}';
    }

}
