package com.lambdas.ordenclient.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author kmuniz
 */
public class ArticuloPK implements Serializable{
    
    private Long idArticulo;
    private String codigoArticulo;

    public ArticuloPK() {
    }

    public Long getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.idArticulo);
        hash = 89 * hash + Objects.hashCode(this.codigoArticulo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloPK other = (ArticuloPK) obj;
        if (!Objects.equals(this.codigoArticulo, other.codigoArticulo)) {
            return false;
        }
        return Objects.equals(this.idArticulo, other.idArticulo);
    }

    @Override
    public String toString() {
        return "ArticuloPK{" + "idArticulo=" + idArticulo + ", codigoArticulo=" + codigoArticulo + '}';
    }
    
}
