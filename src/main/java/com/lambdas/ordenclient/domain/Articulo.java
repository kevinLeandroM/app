package com.lambdas.ordenclient.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author kmuniz
 */

@Entity
@Table(name = "ARTICU_PRODUCT", schema = "producer")
@NamedQuery(name = "findArticuloDes", query = "SELECT ar FROM Articulo ar ORDER BY ar.idArticulo DESC")
@NamedQuery(name = "findArticuloByCode", query = "SELECT ar FROM Articulo ar WHERE ar.codigoArticulo = :codeIn")
public class Articulo implements Serializable{
    
    @Id
    @GeneratedValue(generator = "seq_id_articl_product", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "seq_id_articl_product", sequenceName = "seq_id_articl_product", allocationSize = 19, schema = "producer")
    @Column(name = "ID_ARTICU")
    private Long idArticulo;
    
    @Basic
    @Column(name = "CODG_ARTIC", length = 20, unique = true)
    @NotNull(message = "El codigo del Articulo no puede estar vacio")
    private String codigoArticulo;
    
    @Basic
    @Column(name = "NOMB_ARTIC", length = 80)
    @NotNull(message = "El nombre del Articulo no puede estar vacio")
    private String nombre;
    
    @Basic
    @Column(name = "PREC_INDV_ARTIC", scale = 8, precision = 2)
    @NotNull(message = "El precio unitario del Articulo no puede estar vacio")
    private Double precioUnitario;

    public Articulo() {
    }

    public Articulo(String codigoArticulo, String nombre, Double precioUnitario) {
        this.codigoArticulo = codigoArticulo;
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
    }

    public Articulo(Long idArticulo, String codigoArticulo, String nombre, Double precioUnitario) {
        this.idArticulo = idArticulo;
        this.codigoArticulo = codigoArticulo;
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
    }

    public Long getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.idArticulo);
        hash = 67 * hash + Objects.hashCode(this.codigoArticulo);
        hash = 67 * hash + Objects.hashCode(this.nombre);
        hash = 67 * hash + Objects.hashCode(this.precioUnitario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Articulo other = (Articulo) obj;
        if (!Objects.equals(this.codigoArticulo, other.codigoArticulo)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.idArticulo, other.idArticulo)) {
            return false;
        }
        return Objects.equals(this.precioUnitario, other.precioUnitario);
    }

    @Override
    public String toString() {
        return "Articulo{" + "idArticulo=" + idArticulo + ", codigoArticulo=" + codigoArticulo + ", nombre=" + nombre + ", precioUnitario=" + precioUnitario + '}';
    }
    
}
