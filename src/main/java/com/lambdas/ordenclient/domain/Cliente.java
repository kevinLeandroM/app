package com.lambdas.ordenclient.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author kmuniz
 */
@Entity
@Table(name = "CLIENT_PRODUCT", schema = "producer")
@NamedQuery(name = "findLastClient", query = "SELECT c FROM Cliente c ORDER BY c.idCliente DESC")
public class Cliente implements Serializable{
   
    @Id
    @GeneratedValue(generator = "seq_id_client_product", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "seq_id_client_product", sequenceName = "seq_id_client_product", allocationSize = 19, schema = "producer")
    @Column(name = "ID_CLIENT")
    private Long idCliente;
    
    @Basic
    @Column(name = "NOMB_CLIENT", length = 80)
    private String nombre;
    
    @Basic
    @Column(name = "APEL_CLIENT", length = 80)
    private String apellido;
    
    @Basic
    @Column(name = "CI_RUC_CLIENT", length = 15)
    private String ciRuc;

    public Cliente() {
    }

    public Cliente(Long idCliente, String nombre, String apellido, String ciRuc) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciRuc = ciRuc;
    }

    public Cliente(String nombre, String apellido, String ciRuc) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciRuc = ciRuc;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCiRuc() {
        return ciRuc;
    }

    public void setCiRuc(String ciRuc) {
        this.ciRuc = ciRuc;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.idCliente);
        hash = 67 * hash + Objects.hashCode(this.nombre);
        hash = 67 * hash + Objects.hashCode(this.apellido);
        hash = 67 * hash + Objects.hashCode(this.ciRuc);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        if (!Objects.equals(this.ciRuc, other.ciRuc)) {
            return false;
        }
        return Objects.equals(this.idCliente, other.idCliente);
    }

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", nombre=" + nombre + ", apellido=" + apellido + ", ciRuc=" + ciRuc + '}';
    }
    
}
