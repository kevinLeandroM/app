package com.lambdas.ordenclient.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author kmuniz
 */
@Entity
@Table(name = "ORDER_COMPR_CAB_PRODUCT", schema = "producer")
@NamedQuery(name = "findOrderCompraDescById", query = "SELECT oc FROM OrdenCompra oc ORDER BY oc.idOrdenCompra DESC")
public class OrdenCompra implements Serializable{
    
    @Id
    @GeneratedValue(generator = "seq_id_orderCompraCab_product", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "seq_id_orderCompraCab_product", sequenceName = "seq_id_orderCompraCab_product", allocationSize = 19, schema = "producer")
    @Column(name = "ID_ORDER_COMPRA")
    private Long idOrdenCompra;
    
    @Basic
    @Column(name = "FECH_HORA_ORDER_COMPRA")
    private LocalDate fechaHora;
    
    @OneToOne
    @JoinColumn(name = "ID_CLIENT_ORDER_COMPRA", referencedColumnName = "ID_CLIENT")
    private Cliente cliente;
    
    @Basic
    @Column(name = "TOTTAL_ORDER_COMPRA", precision = 8, scale = 2)
    private Double total;

    public OrdenCompra() {
    }

    public OrdenCompra(LocalDate fechaHora, Cliente cliente, Double total) {
        this.fechaHora = fechaHora;
        this.cliente = cliente;
        this.total = total;
    }

    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public LocalDate getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(LocalDate fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.idOrdenCompra);
        hash = 83 * hash + Objects.hashCode(this.fechaHora);
        hash = 83 * hash + Objects.hashCode(this.cliente);
        hash = 83 * hash + Objects.hashCode(this.total);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdenCompra other = (OrdenCompra) obj;
        if (!Objects.equals(this.idOrdenCompra, other.idOrdenCompra)) {
            return false;
        }
        if (!Objects.equals(this.fechaHora, other.fechaHora)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        return Objects.equals(this.total, other.total);
    }

    @Override
    public String toString() {
        return "OrdenCompra{" + "idOrdenCompra=" + idOrdenCompra + ", fechaHora=" + fechaHora + ", cliente=" + cliente + ", total=" + total + '}';
    }
    
}
