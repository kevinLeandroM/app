<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Altiora Corp</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="../bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/DentiApp_min.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/jquery-cookie/jquery.cookie.js"></script>
    <script src="../assets/core/model/articulo_m.js"></script>
    <script src="../assets/core/logical/init.js"></script>
    <script src="../assets/core/logical/articulo.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
      <a href="#" class="logo">
        <span class="logo-mini"><b>A</b>Corp</span>
        <span class="logo-lg"><b>Altiora</b>Corp</span>
    </a>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li><a href="cliente.php"><i class="fa fa-users"></i> <span>Cliente</span></a></li>
            <li class="active"><a href="producto.php"><i class="fa fa-shopping-cart"></i> <span>Articulo</span></a></li>
            <li><a href="ordencompra.php"><i class="fa fa-car"></i> <span>Orden de compra</span></a></li>
        </ul>
    </section>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Articulo<small>Gestión de Articulo</small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Articulo</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <!-- INGRESO -->
        <div class="col-md-4" id="box_i" name="box_i">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Registro de Articulo</h3>
            </div>
            <div class="box-body">
              <div id="notification_alter_insert"></div>

              <div class="form-group">
                <label>Codigo:</label>
                <input autocomplete="off" type="text" class="form-control" id="codigo_i" name="insertData" maxlength="20">
              </div>

              <div class="form-group">
                <label>Nombre:</label>
                <input autocomplete="off" type="text" class="form-control" id="nombre_i" name="insertData" maxlength="80">
              </div>

              <div class="form-group">
                <label>Precio Unitario:</label>
                <div class="input-group">
                  <span class="input-group-addon">$</span>
                  <input autocomplete="off" type="text" class="form-control" id="precio_unitario_i" name="money_i" onkeypress="return validaPoint(event)" maxlength="11">
                </div>
              </div>
              
              <div class="box-footer">
                <button class="btn btn-info pull-deft" id="save" name="save">Guardar</button>
                <button class="btn btn-danger pull-right" id="cancel_i" name="cancel">Cancelar</button>
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="modalDataInformation">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Default Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p id="modal-body-text"></p>
                        <div class="form-group" id="modal-body-textarea">
                            <textarea maxlength="500" class="form-control" id="detail-o-modal" placeholder="Detalle simple"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="button-yes-modal">Si</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lista de Articulo</h3>
            </div>
            <div class="box-body">
              <div id="notification_alter_list"></div>
              <table id="dataTable" name="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="40%">Codigo</th>
                  <th width="40%">Nombre</th>
                  <th width="20%">Precio</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 1.0.0 </div>
</footer>
</div>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<script src="../dist/js/adminlte.min.js"></script>
<script src="../dist/js/demo.js"></script>
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="../bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  $('[data-mask]').inputmask();
  $('#dataTable').DataTable();
</script>
</body>
</html>