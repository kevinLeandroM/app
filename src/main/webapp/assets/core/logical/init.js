let urlDominio = 'http://localhost:8080/OrdenClient/';
let servicesURL  = urlDominio+'api/';
let pagesURL  = urlDominio+'pages/';

function closeSeccion(){
    disableElement();
    setTimeout('irLogin()',5000);
}

function validaNumber(e){
    tecla = (document.all) ? e.keyCode : e.which;
    teclas = (document.all) ? e.charCode :e.keyCode;
    if ((teclas==8)||(teclas==9)||(teclas==13)){
        return true;
    }
    if((tecla>=48)&&(tecla<=57)){ 
        patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);                
    }
    else{
        return false;
    }
}

function validaPoint(e){
    tecla = (document.all) ? e.keyCode : e.which;    
    teclas = (document.all) ? e.charCode :e.keyCode;
    if ((tecla==8)||(tecla==11)||(tecla==46)||(teclas==9)||(teclas==116)){
        return true;
    }       
    if((tecla>=48)&&(tecla<=57)){   
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    else{
        return false;
    }    
}

function validaIdentification(identification){
    let cad = identification.trim();
    let total = 0;
    let longitud = cad.length;
    let longcheck = longitud - 1;

    if (cad !== "" && longitud === 10){
      for(i = 0; i < longcheck; i++){
        if (i%2 === 0) {
          var aux = cad.charAt(i) * 2;
          if (aux > 9) aux -= 9;
          total += aux;
        } else {
          total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
        }
      }
      total = total % 10 ? 10 - total % 10 : 0;
      if (cad.charAt(longitud-1) == total) {
        return true;
      }else{
        return false;
      }
    }
}

function validaPointPercentage(e){
    tecla = (document.all) ? e.keyCode : e.which;    
    teclas = (document.all) ? e.charCode :e.keyCode;
    if ((tecla==8)||(tecla==11)||(tecla==46)||(teclas==9)||(teclas==116)||(tecla==37)){
        return true;
    }       
    if((tecla>=48)&&(tecla<=57)){   
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    else{
        return false;
    }    
}

function dateFormatInputElement(dateReference,yearPush,mountPush,dayPush){
    let now = new Date();
    let year = (now.getFullYear() + yearPush);
    let mount = ("0"+((now.getMonth() + 1 ) + mountPush)).slice(-2);
    let day =  ("0"+(now.getDate()+dayPush)).slice(-2);
    switch(dateReference){
        case 0:
            return day+"-"+mount+"-"+year;
            break;
        case 1:
            return year+"-"+mount+"-"+day;
            break;
        case 2:
            return day+"/"+mount+"/"+year;
            break;
        case 3:
            return year+"/"+mount+"/"+day;
            break;
        case 4:
            return day+"-"+mount+"-"+year+" "+("00"+now.getHours()).slice(-2)+":"+("00"+now.getMinutes()).slice(-2)+":"+("00"+now.getSeconds()).slice(-2);
            break;
        case 5:
            return year+"-"+mount+"-"+day+" "+("00"+now.getHours()).slice(-2)+":"+("00"+now.getMinutes()).slice(-2)+":"+("00"+now.getSeconds()).slice(-2);
            break;
    }
}

function dateFormatText(dateReference){
    let month_names =["Ene","Feb","Mar",
                      "Apr","May","Jun",
                      "Jul","Aug","Sep",
                      "Oct","Nov","Dic"];

    let now = new Date(dateReference);
    let year = now.getFullYear();
    let mount = now.getMonth()+1;
    let day =  now.getDate()+1;

    return "" + day + "-" + month_names[mount-1] + "-" + year;
}

function dateFormat(dateReference){
    let partsDate = dateReference.split("-");
    let now = new Date(partsDate[0], partsDate[1] - 1, partsDate[2]);
    return ("0"+(now.getDate())).slice(-2)+"-"+("0"+((now.getMonth() + 1 ))).slice(-2)+"-"+now.getFullYear();
}

function dateFormatDOM(dateReference){
    let partsDate = dateReference.split("/");
    let now = new Date(partsDate[2], partsDate[1] - 1, partsDate[0]);
    return now.getFullYear()+"-"+(("0"+((now.getMonth() + 1 ))).slice(-2))+"-"+("0"+(now.getDate())).slice(-2);
}

function emptyAllAndUncover(){
    $("select[class='form-control']").val($("select option:first").val());
    $("input[type='text']").val("");
    $("input[type='email']").val("");
    $("input[type='date']").val(dateFormatInputElement(0,0,0,0));
    $("textarea").val("");
    $("input").first().focus();
}

function disableElement(){
    $("input").attr("disabled",true);
    $("button").attr("disabled",true);
    $("select").attr("disabled",true); 
    $("textarea").attr("disabled",true); 
}

function enableElements(){
    $("input").attr("disabled",false);
    $("button").attr("disabled",false);
    $("select").attr("disabled",false); 
    $("textarea").attr("disabled",false); 
}

function populateList(selector,indetifier,nameOption) {
  $(selector).append('<option value="'+indetifier+'">'+nameOption+'</option>');
}

function generateButton(type,title,icon,event,data,disabled){
    return($("<button/>", {
            "type"          : "button",
            "class"         : "btn btn-"+type+" btn-xs",
            "data-toggle"   : "tooltip",
            "data-placement": "top",                
            "title"         : title,
            "disabled"      : disabled,
            "onclick"       : event+"('"+data+"')",
            "html"          : "<i class='fa "+icon+" fa-fw'></i>"
        }));
}

function generateNotification(type,typeId,title){
    return ($("<div/>", {            
        "class"     : "disabled alert alert-"+type+" alert-dismissible",
        "id"        : typeId,
        "html"      : "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>",
        "text"      : title
    }));
}

function generateAndDeleteNotification(type,message,location,event,time){    
    let panelNotification = generateNotification(type,'div_notificacion_temporal',message);
    $('#'+location).append(panelNotification.prop('outerHTML'));  
    
    switch(event){
        case 0:
            disableElement();
            setTimeout("document.location.reload()",time);
            break;
        case 5:
            disableElement();
            setTimeout('location.href = "../analisis/analisis.php"',2000);
            break;
        default:
            setTimeout("$('#div_notificacion_temporal').remove()",time);
            break;
    }
}

function invokeModal(type, headboard, text, event, data){
    // tipo: 0 simple (solo texto), 1 tabla (texto y tabla), 2 tabla y evento (texto, tabla, boton), 3 question (texto y boton), 4 alerta y evento, 5 alerta y evento con datos (texto event anda data), 6 alerta y focus
    if(headboard == null){
        $('#myModalLabel').text('DentiApp - Notificación');
    }
    else{
        $('#myModalLabel').text(headboard);
    }

    $('#modalDataInformation').modal({
        show: 'true'
    }); 
    $('#modal-body-text').css({'style':'text-align: justify'});
    switch(type){
        case 0:
            $('#modal-body-table').hide();
            $('#modal-body-textarea').hide();
            $('#modal-footer').hide();
            $('#modal-body-text').text(text);
            break;
        case 1:
            $( "#table_modal tbody tr" ).each( function(){
              this.parentNode.removeChild( this ); 
            });
            
            $('#modal-footer').hide();
            $('#modal-body-textarea').hide();
            $('#modal-body-text').hide();
            $('#modal-body-table').show();            
            
            return data.map(function(author) {
                $('#table_modal > tbody:last-child').append('<tr><td>'+author.ordersecuence+'</td><td>'+author.name+'</td></tr>');
            });

            break;
        case 2:
            $('#modal-footer').show();
            $('#modal-body-textarea').hide();
            $('#modal-body-table').show();
            $('#modal-body-text').text(text);
            $('#button-yes-modal').attr('onClick','eliminar("'+id_p+'")');
            break;
        case 3:
            $('#modal-body-table').hide();
            $('#modal-body-textarea').hide();
            $('#modal-footer').show();            
            $('#modal-body-text').text(text);
            $('#button-yes-modal').attr('onClick',event+'("'+data+'")');
            break;
        case 4:
            $('#modal-body-textarea').hide();
            $('#modal-body-table').hide();
            $('#modal-footer').hide();
            $('#modal-body-text').text(text);
            eval(event+'()');
            break;
        case 5:
            $('#modal-body-textarea').hide();
            $('#modal-body-table').hide();
            $('#modal-footer').hide();
            $('#modal-body-text').text(text);
            eval(event+'('+data+')');            
            break;
         case 6:
            $('#modal-body-table').hide();
            $('#modal-footer').show();            
            $('#modal-body-text').text(text);
            $('#modal-body-textarea').show();
            $('#detail-o-modal').val($("#"+data+"-dt").text());
            $('#button-yes-modal').attr('onClick',event+'("'+data+'")');
            break;
    }    
}

function questionDelete(data){
    invokeModal(3,null,'¿Seguro/a de eliminar este registro?','deleteRow',data);
}

function deleteRow(data){
    let subdata = data.split(',');
    $.ajax({
        url: subdata[0]+"/"+subdata[1],
        type: 'DELETE',
        contentType: 'application/json',
        dataType: 'json',
        success: function (result) {
            switch (result.key){
                case 200:
                case 201:
                        generateAndDeleteNotification('success',result.mensaje,'notification_alter_list',0,1000);
                    break;
                case 400:
                case 409:
                        generateAndDeleteNotification('warning',result.mensaje,'notification_alter_list',1,5000);
                    break;
                default:
                    generateAndDeleteNotification('danger',result.mensaje,'notification_alter_list',1,5000);
                    break;
            }
        }
    }).catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_list',1,5000);
    });
}

function recover(data){
    let subdata = data.split(',');
    $.post(subdata[0],{"option":2,"identifier":subdata[1]},
        function (result){
            if(result=="t"){
                registrarAcciones(subdata[2],2,subdata[1]);
                generateAndDeleteNotification('success','Se cambio el estado (Activo) del Registro','notification_alter_list',0,1000);
            }
            else{
                generateAndDeleteNotification('danger',result,'notification_alter_list',1,2000);
            }
        }
    );
}

function zfill(number, width) {
    let numberOutput = Math.abs(number); /* Valor absoluto del número */
    let length = number.toString().length; /* Largo del número */ 
    let zero = "0"; /* String de cero */  
    
    if (width <= length) {
        if (number < 0) {
             return ("-" + numberOutput.toString()); 
        } else {
             return numberOutput.toString(); 
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString()); 
        }
    }
}
