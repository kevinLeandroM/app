let localServiceURL = servicesURL+"purchase-order";
let clienteServiceURL = servicesURL+"client";
let articuloServiceURL = servicesURL+"article";
        
$(document).ready(function(){
    fetch(clienteServiceURL,{
        method: 'GET'
      })
    .then((resp) => resp.json())
    .then(function(data){
        return data.respuesta.map(function(author) {
            populateList($('#cliente_i'),author.idHash,author.ciRuc+" - "+author.apellido+" "+author.nombre);
            populateList($('#cliente_m'),author.idHash,author.ciRuc+" - "+author.apellido+" "+author.nombre);
        });
    })
    .catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });
    
    fetch(articuloServiceURL,{
        method: 'GET'
      })
    .then((resp) => resp.json())
    .then(function(data){
        return data.respuesta.map(function(author) {
            populateList($('#articulo_i'),author.idArticulo,author.codigo+" - "+author.nombre);
            populateList($('#articulo_m'),author.idArticulo,author.codigo+" - "+author.nombre);
        });
    })
    .catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });
    
    let table = $('#dataTable').DataTable();
    fetch(localServiceURL,{
        method: 'GET'
      })
    .then((resp) => resp.json())
    .then(function(data){
        if(data.key !== 200){
            generateAndDeleteNotification('warning',data.mensaje,'notification_alter_list',1,5000);
        }else{
            return data.respuesta.map(function(author) {
                table.row.add([(author.cliente.ciRuc+" - "+author.cliente.nombre+" "+author.cliente.apellido),author.fechaHora,author.total.toFixed(2),
                    generateButton('info','Modificar Orden','fa-pencil-square-o','load',[localServiceURL,author.idOrden],false).prop('outerHTML')+'  '+
                    generateButton('danger','Eliminar Orden','fa-trash-o','questionDelete',[localServiceURL,author.idOrden],false).prop('outerHTML')]).draw();
            });
        }
    })
    .catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });    

    $("#save").click(function(){
        if ($("#article-procedures tbody tr").length > 0 && $("#articulo_i option:selected").val() !== "" && $("#cliente_i option:selected").val()){
            let listDetail = [];
            $("#article-procedures tbody").find('tr').each(function () {
                listDetail.push({"idArticulo": this.title,"cantidad":$('#'+this.id+'-d').val()});
            });
            console.log(JSON.stringify({"idCliente":$("#cliente_i option:selected").val(),"listDetalle":listDetail}));
            $.ajax({
                url: localServiceURL,
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({"idCliente":$("#cliente_i option:selected").val(),"listDetalle":listDetail}),
                dataType: 'json',
                success: function (result) {
                    switch (result.key){
                        case 200:
                        case 201:
                                generateAndDeleteNotification('success',result.mensaje,'notification_alter_insert',1,2500);
                                table.row.add([(result.respuesta.cliente.ciRuc+" - "+result.respuesta.cliente.nombre+" "+result.respuesta.cliente.apellido),result.respuesta.fechaHora,result.respuesta.total.toFixed(2),
                                    generateButton('info','Modificar Orden','fa-pencil-square-o','load',[localServiceURL,result.respuesta.idOrden],false).prop('outerHTML')+'  '+
                                    generateButton('danger','Eliminar Orden','fa-trash-o','questionDelete',[localServiceURL,result.respuesta.idOrden],false).prop('outerHTML')]).draw();
                                cleanData();
                            break;
                        case 400:
                        case 409:
                                generateAndDeleteNotification('warning',result.mensaje,'notification_alter_insert',1,5000);
                            break;
                        default:
                            generateAndDeleteNotification('danger',result.mensaje,'notification_alter_insert',1,5000);
                            break;
                    }
                }
            }).catch(function(error) {
                generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
            });
        }
        else{
            generateAndDeleteNotification('warning','Compruebe e Intente nuevamente','notification_alter_insert',1,3000);
        }
    });

    $("button[name='cancel']").click(function(){
        emptyAllAndUncover();
        if(this.id === "cancel_i"){
            $('#article-procedures tbody tr').remove();
            calculateTotal();
        }else if(this.id === "cancel_m"){
            $('#article-procedures_2 tbody tr').remove();
            calculateTotalBD();
            $('#box_m').hide();
            $('#box_i').show(); 
        }
    });    
});

let countRowsProceduore = 0;

function addProduct(){
    let articleID = $("#articulo_i option:selected").val();
    fetch(articuloServiceURL+"/"+articleID,{
        method: 'GET'
      })
    .then((resp) => resp.json())
    .then(function(data){
        countRowsProceduore++;
        $("#article-procedures tbody").append("<tr id='r-"+countRowsProceduore+"' title='"+data.respuesta.idArticulo+"'><td style='display:none'>"+data.respuesta.idArticulo+"</td>\n\
            <td id='r-"+countRowsProceduore+"-p'>"+generateButton('danger','Eliminar Articulo','fa-trash-o','questionDeleteProcedure',"r-"+countRowsProceduore,false).prop('outerHTML')+" "+data.respuesta.codigo+"</td>\n\
            <td>"+data.respuesta.nombre+"</td><td id='r-"+countRowsProceduore+"-pu'>"+data.respuesta.precioUnidad.toFixed(2)+"</td>\n\
            <td><input id='r-"+countRowsProceduore+"-d' style='width:70px;' onkeypress='return validaNumber(event)' maxlength='11' value='1' onblur='calculateDescOdonto(this)'></td><td style='text-align: center;' id='r-"+countRowsProceduore+"-t' width='10%'>"+data.respuesta.precioUnidad.toFixed(2)+"</td></tr>");
        calculateTotal();
    })
    .catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });
}

function cleanData(){
    emptyAllAndUncover();
    $('#article-procedures tbody tr').remove();
    calculateTotal();
}

function calculateDescOdonto(element){
    let identifier = (element.id).substring(0,(element.id.length-2));
    if (parseFloat(element.value) <= 0 || element.value == ""){
        element.value = "1";
        $('#'+identifier+'-t').text(parseFloat($("#"+identifier+"-pu").text()).toFixed(2));
    }
    else{
        $('#'+identifier+'-t').text((parseFloat($("#"+identifier+"-pu").text()) * parseFloat(element.value)).toFixed(2));
    }
    calculateTotal();
}

function calculateTotal(){
    let subtotal = 0;  
    if ($("#article-procedures tbody tr").length > 0) {
        $("#article-procedures tbody tr").find('td:eq(5)').each(function () {
            if($.isNumeric($(this).html())){
                subtotal += (parseFloat($(this).html()));
            }        
        });
    }
    $("#total_o").text(subtotal.toFixed(2));
}

function questionDeleteProcedure(identifierParent){
    invokeModal(3,null,'¿Seguro/a de eliminar este procedimiento del listado?','deleteRowProcedure',identifierParent);
}

function deleteRowProcedure(identifierParent){
    $("#"+identifierParent).remove();
    calculateTotal();
}

function load(data){
    let subdata = data.split(',');
    $('#identifier_m').val(subdata[1]);    
    fetch(subdata[0]+'/'+subdata[1])    
    .then((resp) => resp.json())
    .then(function(data){
        $('#cliente_m').val(data.respuesta.cliente.idHash);
        $("#total_m").text(data.respuesta.total.toFixed(2));
        data.respuesta.listDetalle.map(function(author) {
                $("#article-procedures_2 tbody").append("<tr id='rm-"+author.id+"'><td id='rm-"+author.articulo.idArticulo+"-kjsa'style='display:none'>"+author.articulo.idArticulo+"</td>"+
                    "<td id='rm-"+author.id+"-p'>"+
                    generateButton('success','Actualizar Articulo','fa-pencil-square-o','updateProcedureBD',author.id,false).prop('outerHTML')+" "+
                    generateButton('danger','Eliminar Articulo','fa-trash-o','questionDeleteProcedureBD',author.id,false).prop('outerHTML')+"</td>"+
                    "<td>"+author.articulo.nombre+"</td><td id='rm-"+author.id+"-pu'>"+author.articulo.precioUnidad.toFixed(2)+"</td>"+
                    "<td><input id='rm-"+author.id+"-d' style='width:70px;' onkeypress='return validaNumber(event)' maxlength='11' value='"+author.cantidad+"' onblur='calculateDescBD(this)'></td><td style='text-align: center;' id='rm-"+author.id+"-t' width='10%'>"+(parseFloat(author.articulo.precioUnidad) * parseFloat(author.cantidad)).toFixed(2)+"</td></tr>");
                    });
    }).catch(function(error) {
        if(error.responseJSON !== null){
            generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_update',1,5000);
        }else{
            generateAndDeleteNotification('danger','Algo salio mal','notification_alter_update',1,5000);
        }
    });
    $('#box_i').hide();
    $('#box_m').show();
}

function questionDeleteProcedureBD(identifier){
    invokeModal(3,null,'¿Seguro/a de eliminar este procedimiento del listado y de la BD?','deleteRow',[localServiceURL+"/detail",identifier]);
}

function calculateDescBD(element){
    let identifier = (element.id).substring(0,(element.id.length-2));
    if (parseFloat(element.value) <= 0 || element.value == ""){
        element.value = "1";
        $('#'+identifier+'-t').text(parseFloat($("#"+identifier+"-pu").text()).toFixed(2));
    }
    else{
        $('#'+identifier+'-t').text((parseFloat($("#"+identifier+"-pu").text()) * parseFloat(element.value)).toFixed(2));
    }
    calculateTotalBD();
}

function calculateTotalBD(){
    let subtotal = 0;  
    if ($("#article-procedures_2 tbody tr").length > 0) {
        $("#article-procedures_2 tbody tr").find('td:eq(5)').each(function () {
            if($.isNumeric($(this).html())){
                subtotal += (parseFloat($(this).html()));
            }        
        });
    }
    $("#total_m").text(subtotal.toFixed(2));
}

function updateProcedureBD(identifier){
    console.log(JSON.stringify({"id":identifier,"idArticulo":$("#rm-"+identifier+"-kjsa").text(),"cantidad":$("#rm-"+identifier+"-d").val()}));
    $.ajax({
        url: localServiceURL+"/detail",
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify({"id":identifier,"idArticulo":$("#rm-"+identifier+"-kjsa").text(),"cantidad":$("#rm-"+identifier+"-d").val()}),
        dataType: 'json',
        success: function (result) {
            switch (result.key){
                case 200:
                case 201:
                        generateAndDeleteNotification('success',result.mensaje,'notification_alter_update',0,2500);
                    break;
                case 400:
                case 409:
                        generateAndDeleteNotification('warning',result.mensaje,'notification_alter_insert',1,5000);
                    break;
                default:
                    generateAndDeleteNotification('danger',result.mensaje,'notification_alter_insert',1,5000);
                    break;
            }
        }
    }).catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });
}