let localServiceURL = servicesURL+"client";
        
$(document).ready(function(){
    
    let table = $('#dataTable').DataTable();
    
    fetch(localServiceURL,{
        method: 'GET'
      })
    .then((resp) => resp.json())
    .then(function(data){
        if(data.key !== 200){
            generateAndDeleteNotification('warning',data.mensaje,'notification_alter_list',1,5000);
        }else{
            return data.respuesta.map(function(author) {
                table.row.add([author.ciRuc,author.nombre,author.apellido]).draw();
            });
        }
    })
    .catch(function(error) {
        generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
    });    

    $("#save").click(function(){
        if($("#lastName_i").val() !== "" && $("#firstName_i").val() !== "") {
            $.ajax({
                url: localServiceURL,
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(new Cliente(null,$('#identification_i').val()!== null?$('#identification_i').val():"",$("#lastName_i").val(),$("#firstName_i").val())),
                dataType: 'json',
                success: function (result) {
                    switch (result.key){
                        case 200:
                        case 201:
                                generateAndDeleteNotification('success',result.mensaje,'notification_alter_insert',1,2500);
                                table.row.add([result.respuesta.ciRuc,result.respuesta.nombre,result.respuesta.apellido]).draw();
                                emptyAllAndUncover();
                            break;
                        case 400:
                        case 409:
                                generateAndDeleteNotification('warning',result.mensaje,'notification_alter_insert',1,5000);
                            break;
                        default:
                            generateAndDeleteNotification('danger',result.mensaje,'notification_alter_insert',1,5000);
                            break;
                    }
                }
            }).catch(function(error) {
                generateAndDeleteNotification('danger',error.responseJSON.mensaje,'notification_alter_insert',1,5000);
            });
        }
        else{
            generateAndDeleteNotification('warning','Compruebe e Intente nuevamente','notification_alter_insert',1,3000);
        }
    });

    $("button[name='cancel']").click(function(){
        emptyAllAndUncover();
        if(this.id === "cancel_m"){
            $('#box_m').hide();
            $('#box_i').show();
            $('#identification_i').focus();
        }
    });    
});