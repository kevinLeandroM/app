class Articulo{
    
    idArticulo;
    codigo;
    nombre;
    precioUnidad;
    
    constructor(idArticulo_in,codigo_in,nombre_in,precioUnidad_in ) {
        this.idArticulo = idArticulo_in;
        this.codigo = codigo_in;
        this.nombre = nombre_in;
        this.precioUnidad = precioUnidad_in;
    }
    
}
